# Fundamentals of Sound

---

# Frequency and Amplitude

---


![Frequency and amplitude](/home/kf/sc/sound-movement-workshop/images/freq-amp.jpg)

- Frequency = how high or low is the sound?  Unit: Hz 
- Amplitude = how loud or quiet is the sound? Unit: dB

---

# Fundamental Wave Forms

---

### Sine Wave

![Sine wave](images/sinewave.jpg)

---

### White Noise

![White Noise](images/whitenoise.jpg)


--- 

# Envelope

![ASDR envelope](images/adsr-envelope.png)

How does a sound develop in time?

--- 

# SuperCollider

![sc-logo](images/sclogo.jpg)

- Programming language for sound
- Free and open source
- Works with almost any sensor tech

---

# Language and Server

- SC is divided into a language and a server
- We will program on the language side
- The server side makes the actual sound

---

# UGens

The server consists of objects that we plug together to build synthesizers:

- sine waves
- noise generators
- filters
- delays
- and so on...

---

#  Creating Synths

To make sounds we need to create synths. To do this we:

1. Write a SynthDef, the basic unit of our program
2. Send the SynthDef to the server
3. Make new Synths based on the SynthDef

---

# Arguments and Variables

- An argument is a parameter that we give to the synthesizer, fx frequency or amplitude. Can be changed while the synth is running.
- A variable is a name or a label we give to an object (a UGen, a number, a list and so on) that we use when writing the SynthDef.


---

# Setting up ad-hoc network
