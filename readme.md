# Roosna & Flak: Sound and Movement Workshop

Welcome to [Roosna & Flak's](http://roosnaflak.com) Sound and Movement Workshop! In this workshop we will deal with the integration of sound and movement, using the open-source programming language [SuperCollider](https://supercollider.github.io) and movement sensors to generate sound.

Before the workshop starts, you will need to download and install some software to your computer as well as to your phone: SuperCollider, [TouchOSC](https://hexler.net/products/touchosc) and [Audacity](https://www.audacityteam.org/).

## Installing the software

On the SuperCollider homepage you will find the download links for your operating system. Download the correct version for your system, click on the downloaded installer and follow the instructions. In addition to installing SuperCollider we highly recommend that you also install [sc3-plugins](https://supercollider.github.io/sc3-plugins/). If you are a Linux user, we recommend that you install through your distribution's package manager, if this is available. On Arch Linux this is as simple as typing ``sudo pacman -S supercollider sc3-plugins`` into a terminal.

[Audacity](https://www.audacityteam.org/) is a free and open source editor for sound files. You will use this in the workshop for recording your own sounds and cutting them into manageable pieces. Visit the [download](https://www.audacityteam.org/download/) page for instructions on how to install the software to your computer. On Arch Linux, you can type ``sudo pacman -S audacity``.

To get data from your mobile phone to the computer we use an app called [TouchOSC](https://hexler.net/products/touchosc). Unfortunately the software is neither free nor open source, but we are not aware of similar, free apps that work for both mobile platforms. For iPhones you will need to download it from App Store, for Android phones you can find it in Play Store.


## Getting to grips with SuperCollider

If you have never worked with code before, SuperCollider can be an intimidating beast, but the basics of how it works are really rather simple and elegant. During the course of the workshop we will instruct you in how to use the programming language, as well as point you in the right direction if you want to learn more about it. A great starting point is [Eli Fieldsteel's excellent tutorial series](https://www.youtube.com/playlist?list=PLPYzvS8A_rTaNDweXe6PX4CXSGq4iEWYC), guiding you through the confusing world of creative code with a steady hand. You can also read [Scott Wilson and James Harkins' Getting Started with SuperCollider tutorial](http://doc.sccode.org/Tutorials/Getting-Started/00-Getting-Started-With-SC.html) for a different approach.

## Setting up TouchOSC

In order for TouchOSC to send data to SuperCollider, both your phone and your computer need to be on the same wireless network. The easiest way to accomplish this is by creating an ad-hoc network from your laptop. The way to do this differs depending on your operating system. We will do this together in the workshop, so no need to panic if you can't figure this out at home!


### Configuring TouchOSC

Now that you have found your ip address, you can open TouchOSC on your phone and push the tiny little circle in the upper right corner of the screen. This will take you to the configuration settings. The setting we are interested in is the one on the top, OSC. Tap on this to edit some settings. Tap the ``enabled`` button to make it work. In the ``Host`` field you need to fill in the ip address of your computer. In the ``Port (outgoing)`` field you should fill in ``57120``. 

For now, you can ignore the ``Port (incoming)`` field.

For a friendly tutorial for setting up TouchOSC with macOS you can check out [Eli Fieldsteel's youtube video](https://www.youtube.com/watch?v=ZVTbRNu2BI0)


## Workshop resources

Go to the [workshop homepage](https://gitlab.com/kflak/sound-and-movement-workshop), click on the little cloud and arrow symbol on the right-hand side, and download the files as a zip archive. Create a folder on your harddrive with an appropriate name, for example ``sound-and-movement-workshop``, move the zip archive into this and unzip it. Usually this involves right-clicking on macOS and Windows. If you use Linux you probably already have a favorite utility for unzipping an archive and strong feelings about how to use it! Once the files have been unzipped feel free to trash the zip archive.

